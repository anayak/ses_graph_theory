#!/bin/bash
# Run this script in the GWDG Jupyter Cloud Server/your computer within the terminal to locally install dependencies 
# $ source install_requirements.sh
# Note: This script assumes tar, make, python3, python3-pip are already available.
#
# variables
GZ_VER=8.1.0
GZ_PATH=${HOME}/opt/graphviz-${GZ_VER}
GZ_LINK="https://gitlab.com/api/v4/projects/4207231/packages/generic/graphviz-releases/${GZ_VER}/graphviz-${GZ_VER}.tar.gz"
#
# download graphviz source
mkdir -p ${GZ_PATH}
wget ${GZ_LINK}
# compile and ipython3install graphviz
echo "extracting downloaded contents..."
tar xzf graphviz-${GZ_VER}.tar.gz
cd graphviz-${GZ_VER}/
./configure --prefix=${GZ_PATH}
make
make install
# remove downloaded source files
cd ../
rm -rf graphviz-${GZ_VER}
# make sure pip can find locally installed graphviz
export CPATH="${CPATH:+${CPATH}:}${GZ_PATH}/include"
export LIBRARY_PATH="${LIBRARY_PATH:+${LIBRARY_PATH}:}${GZ_PATH}/lib"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}${GZ_PATH}/lib"
export PATH="${PATH:+${PATH}:}${GZ_PATH}/bin"
# install pygraphviz and other dependencies
pip install --user jupyter pygraphviz networkx[default,extra]