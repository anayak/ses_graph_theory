import networkx as nx
from networkx import nx_agraph as nxa
from IPython.display import SVG

def draw(G, engine='graphviz', layout='dot'):
    """ 
    Draw a networkx graph using matplotlib/PyGraphViz interface (for Jupyter Notebook)
    layout options are [‘neato’|’dot’|’twopi’|’circo’|’fdp’|’nop’]
    """
    if engine=='graphviz':
        H = nxa.to_agraph(G)
        H.layout(prog=layout)
        return SVG(H.draw(format='svg'))
    elif engine=='matplotlib':
        return nx.draw(G, node_color='lightblue', node_size=1000, with_labels=True)
    else:
        raise ValueError(f'Rendering engine {engine} not recognized!')