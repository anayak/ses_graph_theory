# Graph theory basics with networkx

8th IMPRS Summer School - Sustainable Energy Systems, Magdeburg.

**S. Grundel, A. S. Nayak**

![](img/qr128.png)

# Installing requirements

Ensure a Python (3.6+) interpreter is available alongwith [Jupyter](https://jupyter.org) to be able to run the notebooks. This may be possible either natively or through a freely-available commercial distribution like [Anaconda](https://www.anaconda.com).

Additional python packages are necessary:
- [networkx](https://networkx.org/)
- [pygraphviz](https://pygraphviz.github.io/) (Preferable, but optional)

A handy bash script [install_requirements.sh](./install_requirements.sh) is provided to install these in a Linux machine with no administrator access.